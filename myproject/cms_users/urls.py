from django.urls import path
from . import views

urlpatterns = [
    path('', views.index),
    path('loggedIn', views.loggedIn),
    path('logout', views.logout_vista),
    path('<str:llave>', views.get_resource),
]