from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import logout
from .models import Contenido


# Create your views here.

def index(request):
    list_content = Contenido.objects.all()
    context = {'list_content': list_content,
               'authenticated': request.user.is_authenticated}
    return render(request, 'cms/index.html', context)


@csrf_exempt
def get_resource(request, llave):
    if request.method == "PUT":
        valor = request.body.decode('utf-8')
    elif request.method == "POST":
        valor = request.POST['valor']
    if request.method == "PUT" or request.method == "POST":
        try:
            c = Contenido.objects.get(clave=llave)
            c.valor = valor
        except Contenido.DoesNotExist:
            c = Contenido(clave=llave, valor=valor)
        c.save()
    context = {'authenticated': request.user.is_authenticated,
               'username': request.user.username}
    return render(request, 'cms/content.html', context)


def loggedIn(request):
    if request.user.is_authenticated:
        logged = 'Bienvenido ' + request.user.username
    else:
        logged = "No estás autenticado. <a href='/admin/'>Autentícate</a>"
    return HttpResponse(logged)


def logout_vista(request):
    logout(request)
    return redirect('/cms/')




